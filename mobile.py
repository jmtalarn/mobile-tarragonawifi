import webapp2

from home.urls  import *
from news.urls import *
from hotspot.urls import *

urls = homeurls + newsurls +hotspoturls # + adminurls

app = webapp2.WSGIApplication(urls ,debug=True)