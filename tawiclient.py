import urllib

from google.appengine.api import urlfetch

from lxml import etree

import logging
logger = logging.getLogger()


class TaWiClient:
  headers={'HTTP_X_REQUESTED_WITH': 'xmlhttprequest','APPID' : 'mobile-tarragonawifi'}
  base_url = "http://www.tarragonawifi.com"
  ##base_url = "http://localhost:8000"
  form_fields = {}
  def getHotspotDetail(self,hotspot_id):
      url = self.base_url + "/hotspots/"+str(hotspot_id)+"/"
      #form_data = urllib.urlencode(form_fields)
      result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.POST,
                    headers=self.headers, deadline=10)
      if result.status_code == 200:
         return result.content
      else:
         return None
         
  def voteHotspot(self,vote,hotspot_id):
    url = self.base_url + "/thumb/"+vote+"/hotspots/" + hotspot_id +"/";
    result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.POST,
                    headers=self.headers, deadline=10)
    if result.status_code == 200:
        return result.content
    else:
        return None    
    
  def searchLocation(self,queried_text):
      url = self.base_url + "/hotspots/search/"+urllib.quote(queried_text.encode('utf-8'))+"/"
      result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.POST,
                    headers=self.headers, deadline=10)
      if result.status_code == 200:
         return result.content
      else:
         return None
 
  def searchLatLng(self,lat,lng):
      url = self.base_url + "/hotspots/search/"+str(lat)+"/"+str(lng)+"/"
      result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.POST,
                    headers=self.headers, deadline=10)
      if result.status_code == 200:
         return result.content
      else:
         return None
         
  def searchByText(self,queried_text):
      url = self.base_url + "/hotspots/list/?q="+urllib.quote(queried_text.encode('utf-8'))+"/"
     
      result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.POST,
                    headers=self.headers, deadline=10)
      if result.status_code == 200:
         return result.content
      else:
         return None

  def newsList(self):
      url = self.base_url + "/news/list/"
     
      result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.POST,
                    headers=self.headers, deadline=10)
      if result.status_code == 200:
         return result.content
      else:
         return None
     
  def getArticleDetail(self,article_id):
      url = self.base_url + "/news/"+str(article_id)+"/"
      #form_data = urllib.urlencode(form_fields)
      result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.POST,
                    headers=self.headers, deadline=10)
      if result.status_code == 200:
         return result.content
      else:
         return None    
  
  def getAboutInfo(self):
      url = self.base_url + "/about/"
      #form_data = urllib.urlencode(form_fields)
      result = urlfetch.fetch(url=url,
                    #payload=form_data,
                    method=urlfetch.GET,
                    headers=self.headers,deadline=10)
      if result.status_code == 200:
      
         tree = etree.HTML(result.content)
         logger.info(etree.tostring(tree.xpath("//*[@id='content']")[0], encoding=unicode))
         return etree.tostring(tree.xpath("//*[@id='content']")[0], encoding=unicode)
      else:
         return None
                            
#Test Script                        
#from tawiclient import TaWiClient
#import pprint
#tawi = TaWiClient()
#result = tawi.getHotspotDetail(271001)
#pprint.pprint(result)                        

