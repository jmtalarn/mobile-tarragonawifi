from init import *
from tawiclient import TaWiClient
import json

#For logging
import pprint
import logging
logger = logging.getLogger()

tawi = TaWiClient()

class HotspotBase(MTWHandler):
    def get(self,hotspot_id):
        template_values = { }
        response = json.loads(tawi.getHotspotDetail(hotspot_id))
        template_values = { 'hotspot': response['hotspot'],'score': response['score']}
        template = jinja_environment.get_template('hotspot/hotspot.html')
        self.response.out.write(template.render(template_values))

class HotspotVote(MTWHandler):
    def post(self,vote,hotspot_id):
        response = json.loads(tawi.voteHotspot(vote,hotspot_id))
        #obj  = { 'score': response } 
        self.response.headers['Content-Type'] = 'application/json'   
        self.response.out.write(json.dumps(response))  
       
        
class HotspotSearch(MTWHandler):
    def get(self):
        template_values = { }
        if self.request.get('q'):
           location_searched = self.request.get('q')
           response = json.loads(tawi.searchLocation(location_searched))
           template_values = { 'results': response['results'], 'searched' : location_searched  }
           
        template = jinja_environment.get_template('hotspot/search_location.html')
        self.response.out.write(template.render(template_values))
        
    def post(self):
        location_searched = self.request.get('search')
        response = json.loads(tawi.searchLocation(location_searched))
        template_values = { 'results': response['results'], 'searched' : location_searched  }
        template = jinja_environment.get_template('hotspot/search_location.html')
        self.response.out.write(template.render(template_values))

class HotspotText(MTWHandler):
    def get(self):
        template_values = { }
        if self.request.get('q'):
           location_searched = self.request.get('q')
           response = json.loads(tawi.searchByText(location_searched))
           template_values = { 'results': response['results'], 'searched' : location_searched  }
           
        template = jinja_environment.get_template('hotspot/search_text.html')
        self.response.out.write(template.render(template_values))
        
    def post(self):
        location_searched = self.request.get('search')
        response = json.loads(tawi.searchByText(location_searched))
        template_values = { 'results': response['results'], 'searched' : location_searched  }
        template = jinja_environment.get_template('hotspot/search_text.html')
        self.response.out.write(template.render(template_values))
        
class HotspotMap(MTWHandler):
    def get(self):
        template = jinja_environment.get_template('hotspot/map.html')
        self.response.out.write(template.render())

class HotspotMapLatLng(MTWHandler):
    def get(self,lat,lng):
        template_values = { 'position' : {'lat': lat, 'lng': lng}  }        
        template = jinja_environment.get_template('hotspot/map.html')
        self.response.out.write(template.render(template_values))

    def post(self,lat,lng):
        response = json.loads(tawi.searchLatLng(lat,lng))
        self.response.headers['Content-Type'] = 'application/json'   
        obj  = { 'results': response['results'] } 
        self.response.out.write(json.dumps(obj))          

 

        