from hotspot.base  import *

hotspoturls = [('/hotspot/(\d+)/',HotspotBase),
               ('/hotspot/search/location',HotspotSearch),
               ('/hotspot/search/text',HotspotText),
               ('/hotspot/map',HotspotMap),
               ('/hotspot/map/(?P<lat>-?\d+\.\d+)/(?P<lng>-?\d+\.\d+)',HotspotMapLatLng),
               ('/thumb/(?P<vote>-?\w+)/hotspots/(?P<hotspot_id>-?\d+)/',HotspotVote)]