from init import *
from tawiclient import TaWiClient
import json

import pprint
import logging
logger = logging.getLogger()

tawi = TaWiClient()

class ArticleBase(MTWHandler):
    def get(self,article_id):
        template_values = { }
        response = json.loads(tawi.getArticleDetail(article_id))
        template_values = { 'article': response['article']}

        template = jinja_environment.get_template('news/article.html')
        self.response.out.write(template.render(template_values))
        
class ArticleList(MTWHandler):
    def articleList(self):
        template_values = { }
        response = json.loads(tawi.newsList())
        template_values = { 'results': response['results'] }
           
        template = jinja_environment.get_template('news/list.html')
        self.response.out.write(template.render(template_values))
        
    def get(self):
        self.articleList()
        
    def post(self):
        self.articleList()
