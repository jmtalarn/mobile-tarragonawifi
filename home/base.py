from init import *

from tawiclient import TaWiClient
tawi = TaWiClient()

class HomeBase(MTWHandler):
    def get(self):
        template_values = { }

        template = jinja_environment.get_template('home/index.html')
        self.response.out.write(template.render(template_values))

class AboutBase(MTWHandler):
    def get(self):
        template_values = { 'about_text': tawi.getAboutInfo()}
        
        template = jinja_environment.get_template('home/about.html')
        self.response.out.write(template.render(template_values))