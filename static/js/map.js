function initialize() {
    
    //var latlng = new google.maps.LatLng(41.1166667, 1.25); //Latitud i Longitud de Tarragona	
    //var latlng = new google.maps.LatLng(lat, lng); //Latitud i Longitud de Tarragona
    
    var mapOptions = {
	  //center: latlng,
	  zoom: 15,
	  mapTypeId: google.maps.MapTypeId.ROADMAP,
	  streetViewControl: false,
	  zoomControlOptions: { style: google.maps.ZoomControlStyle.SMALL, position: google.maps.ControlPosition.TOP_LEFT }
    };
    theMap = new google.maps.Map(document.getElementById("map"), mapOptions);
    defaultLatLng = new google.maps.LatLng(41.1166667, 1.25); //Latitud i Longitud de Tarragona
    

    
    if (typeof(ubicacio) === 'undefined'){
	    // Try W3C Geolocation (Preferred)
	    if(navigator.geolocation) {
	      var location_timeout = setTimeout("handleNoGeolocation(defaultLatLng)", 7000);

	      browserSupportFlag = true;
	      navigator.geolocation.getCurrentPosition(
	    		  function(position){
	    			  	clearTimeout(location_timeout);
	    	  			theMap.setCenter(new google.maps.LatLng(position.coords.latitude,position.coords.longitude));
	    		  }, function(){
	    			  	clearTimeout(location_timeout);
	    			  	handleNoGeolocation(defaultLatLng);		  
	    		  });
	    }else{
	    	handleNoGeolocation(defaultLatLng);
	    }
    }else{
    	theMap.setCenter(new google.maps.LatLng(ubicacio.lat, ubicacio.lng));
    }


};
function handleNoGeolocation(latlng) {
	ubicacio = { lat: latlng.lat(), lng: latlng.lng() };
 	theMap.setCenter(latlng);
}
function loadPuntsWifi(){
	  $.ajax({type:"POST",
			  url: "/hotspot/map/"+theMap.getCenter().lat()+ "/" + theMap.getCenter().lng(),
			  success:  function(data,status){
				  			for (var i = 0; i < puntsWifi.length; i++ ) {
				  				puntsWifi[i].setMap(null);
				  			}  
				  			puntsWifi = [];
				  			for(var i=0;i<data.results.length;i++){
				  				hotspot = data.results[i];
				  				puntWifi = new google.maps.Marker(
				  						{position: new google.maps.LatLng(hotspot.lat,hotspot.long) , 
				  							map: theMap,
				  							title: hotspot.name,
				  							icon: '/img/pin-logo.png'});
				  				puntWifi.hotspot_id = hotspot.id;
				  				puntsWifi.push(puntWifi);
				  				google.maps.event.addListener(puntWifi, 'click', function() {
				  					window.location = "/hotspot/"+this.hotspot_id+"/";
				  				});
				  			}
			  			},
			  	dataType: "json"
	  		});
};
//$(document).ready(function() {
$(document).delegate('#page1', 'pageshow', function () {
  puntsWifi = []
  initialize();
  google.maps.event.addListener(theMap, 'center_changed', function() {
		  	// do something only the first time the map is loaded
	  if (!(typeof(loadPuntsWifi_timeout) === 'undefined')){
		  clearTimeout(loadPuntsWifi_timeout);
	  }
	  loadPuntsWifi_timeout = setTimeout("loadPuntsWifi()", 3000);

   });
 });