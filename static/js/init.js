$.mobile.page.prototype.options.backBtnText = "Enrere";

//url_to_share = encodeURIComponent(document.URL.replace(/m.tarragonawifi/gi,'www.tarragonawifi'));

var url_base = "http://www.tarragonawifi.com";
var url_pathname_params = location.pathname;
if (url_pathname_params=="/hotspot/map"){
  url_base = "http://m.tarragonawifi.com";
}else if (url_pathname_params=="/hotspot/search/text"){
  url_pathname_params = "/hotspots/list/"+location.search;
}else if (url_pathname_params=="/hotspot/search/location"){
  url_pathname_params = "/hotspots/search/"+location.search.split("?q=")[1]+"/";
}else if (url_pathname_params.match(/\/hotspot\/[0-9]+/)!=null){
  url_pathname_params = "/hotspots/"+url_pathname_params.split("/")[2]+"/";
}

url_to_share = encodeURIComponent(url_base + url_pathname_params);

$(document).delegate('#page1', 'pageinit', function () {
	$("a.tweet.button").attr("href","https://twitter.com/share?url="+url_to_share );
	$("a.fb.button").attr("href","http://www.facebook.com/sharer.php?u="+url_to_share );
	$("a.googleplus.button").attr("href","https://plus.google.com/share?url="+url_to_share );
 
	loadCounters();
});

function loadCounters(){  
  $.getJSON('http://urls.api.twitter.com/1/urls/count.json?url='+url_to_share, function(data) {
	$("a.tweet.counter").text(data.count);
  });
  //$.getJSON('http://graph.facebook.com/'+url_to_share+'&callback=?', function(data) {
  $.getJSON('http://graph.facebook.com/'+url_to_share+'', function(data) {
	$("a.fb.counter").text(data.shares);
  });
  
  $("a.googleplus.counter").load('https://plusone.google.com/_/+1/fastbutton?url='+url_to_share+' div#aggregateCount'); 
};
