function initialize(lat,lng) {
	  
		var latlng = new google.maps.LatLng(lat, lng);
		var mapOptions = {
	      center: latlng,
	      zoom: 18,
	      mapTypeId: google.maps.MapTypeId.ROADMAP,
		  streetViewControl: false,
		  zoomControlOptions: { style: google.maps.ZoomControlStyle.SMALL, position: google.maps.ControlPosition.TOP_LEFT }

	    };
	    var theMap = new google.maps.Map(document.getElementById("map"), mapOptions);
	    
	    
		 
		var marker = new google.maps.Marker({
			position: latlng,
			map:  theMap,
			//title: name,
			icon: '/img/pin-logo-glow.png'
		});
		initialized_map = true;
	 };
initialized_map= false;

$(document).delegate('#page1', 'pageinit', function () {
	 
	 $( "select#address" ).bind( "change", function(event, ui) { 

		 if (event.target.selectedIndex==1){
			 $('div#map').css('display','block');
			 if (!initialized_map){
			  	 initialize(ubicacio.lat,ubicacio.lng);
			 }
		 }else{
			 $('div#map').css('display','none');			 
		 }
	 });
//	 initialize(ubicacio.lat,ubicacio.lng);
});	  